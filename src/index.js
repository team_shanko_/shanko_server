"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv").config();
var cors = require("cors");
var user_1 = __importDefault(require("./lib/user"));
var room_1 = __importDefault(require("./lib/room"));
var rooms_1 = __importDefault(require("./lib/rooms"));
var db_1 = __importDefault(require("./utils/db"));
var app = require("express")();
app.use(cors());
app.options('*', cors()); // include before other routes
var http = require("http").createServer(app);
var io = require("socket.io")(http, {
    pingInterval: 2000,
    pingTimeout: 5000,
});
io.origins(function (origin, callback) {
    callback(null, true);
});
app.get("/", function (req, res) { return res.send("v1.0.0"); });
// initialize thingy
init();
function init() {
    return __awaiter(this, void 0, void 0, function () {
        var rooms;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    rooms = new rooms_1.default();
                    return [4 /*yield*/, rooms.init()];
                case 1:
                    _a.sent();
                    room_1.default.sockets = io;
                    //endregion
                    io.on("connection", function (socket) {
                        var user;
                        var emit = function (type, payload) {
                            // console.log(`[--> ${user?.sid}]`, type);
                            socket.emit(type, payload);
                        };
                        var broadcast = function (type, payload, incSender) {
                            if (incSender === void 0) { incSender = false; }
                            // console.log(`[--> channel]`, type);
                            if (!user.__room)
                                return;
                            if (incSender) {
                                io.sockets.to(user.__room.roomnumber + "").emit(type, payload);
                                io.sockets.to(user.__room.roomnumber + "s").emit(type, payload);
                            }
                            else {
                                socket.broadcast.to(user.__room.roomnumber + "").emit(type, payload);
                                socket.broadcast.to(user.__room.roomnumber + "s").emit(type, payload);
                            }
                        };
                        socket.on("disconnect", function () {
                            if (user)
                                user._isDisconnected = true;
                        });
                        socket.on("rqst_login", function (_a) {
                            var id = _a.id, password = _a.password;
                            return __awaiter(_this, void 0, void 0, function () {
                                var sql, result, existingUser, e_1;
                                return __generator(this, function (_b) {
                                    switch (_b.label) {
                                        case 0:
                                            _b.trys.push([0, 2, , 3]);
                                            sql = "select * from users where username = '" + id + "' and `password` = password('" + password + "')";
                                            return [4 /*yield*/, db_1.default(sql)];
                                        case 1:
                                            result = _b.sent();
                                            if (result.length == 0) {
                                                socket.emit("resp_login", { retcode: 1 });
                                            }
                                            else {
                                                delete result[0].password;
                                                existingUser = rooms.findUserBySid(result[0].id);
                                                if (existingUser) {
                                                    console.log("found existing user");
                                                    user = existingUser;
                                                    user.__socket = socket;
                                                }
                                                else {
                                                    console.log("creating new user");
                                                    user = new user_1.default(result[0], socket);
                                                }
                                                emit("resp_login", { retcode: 0, sid: user.sid });
                                            }
                                            return [3 /*break*/, 3];
                                        case 2:
                                            e_1 = _b.sent();
                                            console.error("something went wrong", e_1);
                                            socket.emit("resp_login", { retcode: 1 });
                                            return [3 /*break*/, 3];
                                        case 3: return [2 /*return*/];
                                    }
                                });
                            });
                        });
                        socket.on("rqst_userinfo", function () {
                            return emit("resp_userinfo", __assign({ retcode: 0 }, user.forJSON({ reveal: true })));
                        });
                        socket.on("rqst_changegender", function (data) { return __awaiter(_this, void 0, void 0, function () {
                            var _;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, user.changeGender(data.gender)];
                                    case 1:
                                        _ = _a.sent();
                                        emit("resp_changegender", { retcode: 0 });
                                        return [2 /*return*/];
                                }
                            });
                        }); });
                        socket.on("rqst_changeimgnumber", function (data) { return __awaiter(_this, void 0, void 0, function () {
                            var _;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, user.changeImgnumber(data.imgnumber)];
                                    case 1:
                                        _ = _a.sent();
                                        emit("resp_changeimgnumber", { retcode: 0 });
                                        return [2 /*return*/];
                                }
                            });
                        }); });
                        socket.on("rqst_rooms", function () {
                            var roomlist = rooms.rooms.map(function (r) { return (__assign(__assign({}, r), { players: r.players.filter(function (v) { return v; }).length })); });
                            emit("resp_rooms", { retcode: 0, roomlist: roomlist });
                        });
                        socket.on("rqst_room_enter", function (data) {
                            if (!user.__room) {
                                user.__room = rooms.findByRoomnumber(data.roomnumber);
                            }
                            emit("resp_room_enter", {
                                retcode: 0,
                                roomnumber: user.__room.roomnumber,
                            });
                        });
                        socket.on("rqst_ingame_state", function () {
                            var _a;
                            return emit("resp_ingame_state", __assign({ retcode: 0 }, (_a = user.__room) === null || _a === void 0 ? void 0 : _a.forJSON(user.sid)));
                        });
                        socket.on("rqst_ingame_imready", function () {
                            if (!user.__room)
                                return;
                            if (user.__room.getPlayerBySid(user.sid)) {
                                // resuming
                                user._isDisconnected = false;
                                socket.join(user.__room.roomnumber + "");
                            }
                            else {
                                socket.join(user.__room.roomnumber + "s");
                            }
                            emit("resp_ingame_imready", { retcode: 0 });
                        });
                        socket.on("rqst_ingame_sit", function (_a) {
                            var seatIndex = _a.seatIndex;
                            var _b;
                            return (_b = user.__room) === null || _b === void 0 ? void 0 : _b.onSit(user, seatIndex);
                        });
                        socket.on("rqst_ingame_leave", function () {
                            var _a;
                            emit("resp_ingame_leave", { retcode: 0 });
                            (_a = user.__room) === null || _a === void 0 ? void 0 : _a.onLeaveRqst(user);
                        });
                        socket.on("rqst_ingame_leavecancel", function () {
                            user.isLeaving = false;
                            emit("resp_ingame_leavecancel", { retcode: 0 });
                        });
                        socket.on("sresp_ingame_deal", function () { return (user._responded = true); });
                        socket.on("sresp_ingame_place_bet", function (data) { return user.__room && user.__room.onBet({ user: user, data: data }); });
                        socket.on("sresp_ingame_deal", function () { return (user._responded = true); });
                        socket.on("sresp_ingame_player_action", function (_a) {
                            var action = _a.action;
                            var _b;
                            return (_b = user.__room) === null || _b === void 0 ? void 0 : _b.onPlayerAction({ user: user, action: action });
                        });
                        socket.on("sresp_ingame_player_action_update", function () {
                            user._responded = true;
                        });
                        socket.on("sresp_ingame_three_card", function (data) { var _a; return (_a = user.__room) === null || _a === void 0 ? void 0 : _a.onThreeCard(user, data.threecard); });
                        socket.on("sresp_ingame_three_cards", function () { return (user._responded = true); });
                        socket.on("sresp_ingame_banker_action", function (data) { var _a; return (_a = user.__room) === null || _a === void 0 ? void 0 : _a.onBankerAction(user, data.action); });
                        socket.on("sresp_ingame_result", function () { return (user._responded = true); });
                        // // admin sockets
                        // socket.on("server_reset", () => {
                        //   Lobby.reset();
                        //   Users.reset();
                        // });
                    });
                    return [2 /*return*/];
            }
        });
    });
}
http.listen(process.env.PORT, function () {
    return console.log("v2. server started on " + process.env.PORT);
});
