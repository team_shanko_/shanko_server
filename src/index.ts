require("dotenv").config();
const cors = require("cors");

import User from "./lib/user";
import Room from "./lib/room";
import Rooms from "./lib/rooms";
import runQuery from "./utils/db";

const app = require("express")();
app.use(cors())
app.options('*', cors()) // include before other routes

const http = require("http").createServer(app);
const io = require("socket.io")(http, {
  pingInterval: 2000,
  pingTimeout: 5000,
});
io.origins((origin:any, callback:any) => {
  callback(null, true);
});

app.get("/", (req: any, res: any) => res.send("v1.0.0"));

// initialize thingy

init();

async function init() {
  // initialize with database

  //region setup rooms
  let rooms = new Rooms();
  await rooms.init();
  Room.sockets = io;
  //endregion

  io.on("connection", (socket: SocketIO.Socket) => {
    let user: User;
    
    const emit = (type: string, payload: any) => {
      // console.log(`[--> ${user?.sid}]`, type);
      socket.emit(type, payload);
    };
    const broadcast = (
      type: string,
      payload: any,
      incSender: boolean = false
    ) => {
      // console.log(`[--> channel]`, type);
      if (!user.__room) return;
      if (incSender) {
        io.sockets.to(user.__room.roomnumber + "").emit(type, payload);
        io.sockets.to(user.__room.roomnumber + "s").emit(type, payload);
      } else {
        socket.broadcast.to(user.__room.roomnumber + "").emit(type, payload);
        socket.broadcast.to(user.__room.roomnumber + "s").emit(type, payload);
      }
    };

    socket.on("disconnect", () => {
      if (user) user._isDisconnected = true;
    });

    socket.on("rqst_login", async ({ id, password }) => {
      try {
        const sql = `select * from users where username = '${id}' and \`password\` = password('${password}')`;
        let result: any[] = await runQuery(sql);
        if (result.length == 0) {
          socket.emit("resp_login", { retcode: 1 });
        } else {
          delete result[0].password;
          let existingUser: User | undefined = rooms.findUserBySid(result[0].id);
          if (existingUser) {
            console.log("found existing user");
            user = existingUser;
            user.__socket = socket;
          } else {
            console.log("creating new user");
            user = new User(result[0], socket);
          }
          emit("resp_login", { retcode: 0, sid: user.sid });
        }
      } catch (e) {
        console.error("something went wrong", e);
        socket.emit("resp_login", { retcode: 1 });
      }
    });

    socket.on("rqst_userinfo", () =>
      emit("resp_userinfo", { retcode: 0, ...user.forJSON({ reveal: true }) })
    );
    socket.on("rqst_changegender", async (data) => {
      let _ = await user.changeGender(data.gender);
      emit("resp_changegender", { retcode: 0 });
    });
    socket.on("rqst_changeimgnumber", async (data) => {
      let _ = await user.changeImgnumber(data.imgnumber);
      emit("resp_changeimgnumber", { retcode: 0 });
    });

    socket.on("rqst_rooms", () => {
      let roomlist = rooms.rooms.map((r: Room) => ({
        ...r,
        players: r.players.filter((v) => v).length,
      }));
      emit("resp_rooms", { retcode: 0, roomlist });
    });
    socket.on("rqst_room_enter", (data) => {
      if (!user.__room) {
        user.__room = rooms.findByRoomnumber(data.roomnumber);
      }
      emit("resp_room_enter", {
        retcode: 0,
        roomnumber: user.__room!.roomnumber,
      });
    });
    socket.on("rqst_ingame_state", () =>
      emit("resp_ingame_state", {
        retcode: 0,
        ...user.__room?.forJSON(user.sid),
      })
    );
    socket.on("rqst_ingame_imready", () => {
      if (!user.__room) return;
      if (user.__room.getPlayerBySid(user.sid)) {
        // resuming
        user._isDisconnected = false;
        socket.join(user.__room.roomnumber + "");
      } else {
        socket.join(user.__room.roomnumber + "s");
      }
      emit("resp_ingame_imready", { retcode: 0 });
    });
    socket.on("rqst_ingame_sit", ({ seatIndex }: { seatIndex: number }) =>
      user.__room?.onSit(user, seatIndex)
    );
    socket.on("rqst_ingame_leave", () => {
      emit("resp_ingame_leave", { retcode: 0 });
      user.__room?.onLeaveRqst(user);
    });
    socket.on("rqst_ingame_leavecancel", () => {
      user.isLeaving = false;
      emit("resp_ingame_leavecancel", { retcode: 0 });
    });
    socket.on("sresp_ingame_deal", () => (user._responded = true));
    socket.on(
      "sresp_ingame_place_bet",
      (data) => user.__room && user.__room.onBet({ user, data })
    );
    socket.on("sresp_ingame_deal", () => (user._responded = true));
    socket.on("sresp_ingame_player_action", ({ action }: { action: string }) =>
      user.__room?.onPlayerAction({ user, action })
    );
    socket.on("sresp_ingame_player_action_update", () => {
      user._responded = true; 
    });
    socket.on("sresp_ingame_three_card", (data) =>
      user.__room?.onThreeCard(user, data.threecard)
    );
    socket.on("sresp_ingame_three_cards", () => (user._responded = true));
    socket.on("sresp_ingame_banker_action", (data) =>
      user.__room?.onBankerAction(user, data.action)
    );

    socket.on("sresp_ingame_result", () => (user._responded = true));

    // // admin sockets
    // socket.on("server_reset", () => {
    //   Lobby.reset();
    //   Users.reset();
    // });
  });
}

http.listen(process.env.PORT, () =>
  console.log("v2. server started on " + process.env.PORT)
);
