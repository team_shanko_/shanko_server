// var mysql = require("mysql");
import mysql from 'mysql';

var db:mysql.Pool = mysql.createPool({
  connectionLimit: 10,
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME
});

const runQuery = async (sql: string): Promise<any> => {
  return new Promise((resolve, reject) => {
    db.query(sql, (error, results, fields) => {
      if (error) {
        console.error(error);
        resolve(undefined);
      }
      else resolve(results);
    });
  })
}

// module.exports = runQuery;
export default runQuery;



