interface Card {
  num: string;
  img: string;
  value: number;
}

export default Card;