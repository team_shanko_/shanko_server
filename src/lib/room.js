"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var db_1 = __importDefault(require("../utils/db"));
var INACTION_TIMEOUT = 10000; // for user-inaction
var CHECKER_INTERVAL = 500;
var COINS = [
    1000000,
    500000,
    100000,
    50000,
    20000,
    10000,
    5000,
    1000,
    500,
    100,
    50,
    10,
    5,
    1,
];
var FACES = [
    "A",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "10",
    "J",
    "Q",
    "K",
];
var VALUES = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10];
var SUITS = ["SPADES", "HEARTS", "DIAMONDS", "CLUBS"];
var allCards = [];
SUITS.forEach(function (img) {
    FACES.forEach(function (num, i) {
        allCards.push({ img: img, num: num, value: VALUES[i] });
    });
});
//endregion
var Room = /** @class */ (function () {
    function Room(config, roomnumber) {
        var _this = this;
        this._gamenumber = 0; // number of games played for the current banker
        // game related
        this.phaseIndex = 0;
        this._subPhaseIndex = 0;
        this.status = "open";
        this.bankerIndex = -1;
        this.warning = -1;
        this._betActions = [];
        this._deck = __spreadArrays(allCards);
        this.coins = {}; // bank coins
        this.__bankerHistory = [];
        // db related
        this.__gameId = -1;
        this.rank = config.rank;
        this.minBalance = config.min_balance;
        this.minimumbank = config.minimum_bank;
        this.maxPlayers = config.max_players;
        this.roomnumber = roomnumber;
        this.players = new Array(config.max_players).fill(undefined);
        this.bank = 0;
        this._coldstart = true;
        this._threecard = undefined;
        this._bankerAction = undefined;
        // check for events every second
        setInterval(function () {
            var banker = _this.getPlayerBySid(_this.bankerIndex);
            switch (_this.phaseIndex * 10 + _this._subPhaseIndex) {
                // join phase
                case 0:
                    if (_this._isReady()) {
                        _this._coldstart = false;
                        _this._setPhase(10);
                    }
                    else {
                        _this._coldstart = true;
                    }
                    break;
                // betting phase
                case 10:
                    console.log("new phase", 10);
                    _this._beginGame();
                    _this._waitForRespond(_this.__getPlayers());
                    _this._setPhase(11);
                    break;
                case 11:
                    if (_this._didAllRespond())
                        _this._setPhase(20);
                    break;
                // deal (card-spread) phase
                case 20:
                    console.log("new phase", 20);
                    _this._deal();
                    console.log("deal complete");
                    _this._setPhase(21);
                    break;
                case 21:
                    if (_this._didAllRespond())
                        console.log("21 is done, start phase 30");
                    _this._setPhase(_this._isGameOver() ? 60 : 30);
                    break;
                // player action phase
                case 30:
                    console.log("starting phase 30");
                    _this._broadcastPlayers({ type: "srqst_ingame_player_action" });
                    _this._waitForRespond(_this.__getPlayers().filter(function (p) { return !p._result; }));
                    _this._setPhase(31);
                    break;
                case 31:
                    if (_this._didAllRespond()) {
                        console.log("sending out ingame_action_update");
                        _this._broadcastToAll({
                            type: "srqst_ingame_player_action_update",
                            payload: { actions: _this._betActions },
                        });
                        _this._waitForRespond(_this.__getPlayersAndBanker());
                        _this._setPhase(_this._isGameOver() ? 60 : 40);
                    }
                    break;
                // three cards phase
                case 40:
                    if (_this._didAllRespond()) {
                        console.log("phase 40 ended");
                        banker._responded = false;
                        banker.__socket.emit("srqst_ingame_three_card", _this.forJSON(banker.sid));
                        _this._setPhase(41);
                    }
                    break;
                case 41:
                    if (banker && banker._responded) {
                        console.log("phase 41 ended");
                        _this._broadcastToAll({
                            type: "srqst_ingame_three_cards",
                            payload: { threecard: _this._threecard },
                        });
                        _this._waitForRespond(_this.__getPlayersAndBanker());
                        _this._setPhase(42);
                    }
                    break;
                case 42:
                    if (_this._didAllRespond()) {
                        console.log("phase 42 ended", _this._isGameOver() ? 60 : 50);
                        _this._setPhase(_this._isGameOver() ? 60 : 50);
                    }
                    break;
                // banker phase
                case 50:
                    if (banker) {
                        console.log("Phase 50 started. wait for banker action");
                        banker._responded = false;
                        banker.__socket.emit("srqst_ingame_banker_action", _this.forJSON(banker.sid));
                        _this._setPhase(51);
                    }
                    break;
                case 51:
                    if (banker && banker._responded) {
                        console.log("phase 51 ended");
                        _this._broadcastToAll({
                            type: "srqst_ingame_banker_action_update",
                            payload: { action: _this._bankerAction },
                        });
                        _this._waitForRespond(_this.__getPlayersAndBanker());
                        _this._setPhase(60);
                    }
                    break;
                case 60:
                    // result phase
                    console.log("phase 60");
                    _this._reportResult();
                    _this._waitForRespond(_this.__getPlayersAndBanker());
                    _this._setPhase(61);
                    break;
                case 61:
                    if (_this._didAllRespond()) {
                        // leaving people
                        _this.players.forEach(function (p, i) {
                            if (!p)
                                return;
                            if (p.isLeaving) {
                                p.onGameEnd();
                                // leaving
                                _this._doLeave(p);
                            }
                            p.onGameEnd();
                        });
                        // end of banker
                        // TODO Database
                        _this._setPhase(0);
                    }
                    break;
                default:
                    break;
            }
        }, CHECKER_INTERVAL);
    }
    //region common
    Room.prototype.__getBanker = function () {
        var _this = this;
        return this.players.find(function (p) { return p && p.sid === _this.bankerIndex; });
    };
    Room.prototype.__getPlayers = function () {
        var _this = this;
        return this.players.filter(function (p) { return p !== undefined && p.sid !== _this.bankerIndex && p.isActive; });
    };
    Room.prototype.__getPlayersAndBanker = function () {
        return this.players.filter(function (p) { return p !== undefined && p.isActive; });
    };
    Room.prototype._setPhase = function (p) {
        this.phaseIndex = Math.floor(p / 10);
        this._subPhaseIndex = p % 10;
    };
    Room.prototype.getPlayerBySid = function (sid) {
        return this.players.filter(function (p) { return p; }).find(function (p) { return p && p.sid === sid; });
    };
    Room.prototype._broadcastSpectators = function (_a) {
        var type = _a.type, _b = _a.payload, payload = _b === void 0 ? {} : _b;
        payload = __assign(__assign({}, payload), this.forJSON());
        try {
            Room.sockets.to(this.roomnumber + "s").emit(type, payload);
        }
        catch (e) {
            console.log("ERROR", e);
            console.log("ERROR payload", payload);
        }
    };
    Room.prototype._broadcastPlayers = function (_a) {
        var _this = this;
        var excludeSid = _a.excludeSid, type = _a.type, payload = _a.payload;
        this.__getPlayersAndBanker().forEach(function (p) {
            var piggyback = _this.forJSON(p.sid);
            if (!excludeSid || p.sid !== excludeSid) {
                p.__socket.emit(type, __assign(__assign({}, payload), piggyback), console.log);
            }
        });
    };
    Room.prototype._broadcastToAll = function (_a) {
        var excludeSid = _a.excludeSid, type = _a.type, _b = _a.payload, payload = _b === void 0 ? {} : _b;
        this._broadcastSpectators({ type: type, payload: payload });
        this._broadcastPlayers({ excludeSid: excludeSid, type: type, payload: payload });
    };
    Room.prototype.forJSON = function (targetSid) {
        if (targetSid === void 0) { targetSid = -1; }
        var ret = {};
        for (var key in this) {
            if (this.hasOwnProperty(key)) {
                // if (key.startsWith("_")) continue;
                if (key === "players")
                    continue;
                ret[key] = this[key];
            }
        }
        ret.players = this.players.map(function (p) {
            return p ? p.forJSON({ reveal: p.sid === targetSid }) : undefined;
        });
        // generated properties
        ret.winners = this.players
            .filter(function (p) { return p && p._result === "W"; })
            .map(function (p) { return p.sid; });
        ret.losers = this.players
            .filter(function (p) { return p && p._result === "L"; })
            .map(function (p) { return p.sid; });
        ret.revealed = this.players
            .filter(function (p) { return p && p._isRevealed; })
            .map(function (p) { return p.sid; });
        return ret;
    };
    Room.prototype.onSit = function (user, seatIndex) {
        if (this.players[seatIndex]) {
            user.__socket.emit("resp_ingame_sit", { retcode: 1 });
            return;
        }
        this.players[seatIndex] = user;
        user.seatIndex = seatIndex;
        user.onGameEnd();
        user.__socket.emit("resp_ingame_sit", __assign({ retcode: 0 }, this.forJSON(user.sid)));
        this._broadcastToAll({
            excludeSid: user.sid,
            type: "srqst_ingame_newuser",
            payload: user.forJSON({ reveal: false }),
        });
        return true;
    };
    Room.prototype.onLeaveRqst = function (user) {
        user.isLeaving = true;
        if (this.phaseIndex !== 0)
            return;
        if (user.sid === this.bankerIndex)
            return;
        this._doLeave(user);
    };
    Room.prototype._doLeave = function (user) {
        this._broadcastToAll({
            type: "srqst_ingame_leave",
            payload: { sid: user.sid, roomnumber: this.roomnumber },
        });
        user.__socket.leave(this.roomnumber + "s");
        user.__socket.leave(this.roomnumber + "");
        this.players[user.seatIndex] = undefined;
        user.__room = undefined;
    };
    Room.prototype._didAllRespond = function () {
        return (this.__getPlayersAndBanker().filter(function (p) { return !p._responded; }).length === 0);
    };
    Room.prototype._waitForRespond = function (players) {
        players.forEach(function (p) {
            if (p)
                p._responded = false;
        });
    };
    Room.prototype._isGameOver = function () {
        return this.__getPlayers().filter(function (p) { return !p._result; }).length === 0; // everybody has a result
    };
    //endregion
    //region phase 0
    Room.prototype._isReady = function () {
        var _this = this;
        var numPlayers = this.players.filter(function (p) { return p; }).length;
        if (this.roomnumber === 1001) {
            this.__getPlayers().forEach(function (p) { return console.log(p.balance); });
        }
        // do we have enough players
        if ((numPlayers < 3 && this._coldstart) || numPlayers < 2)
            return false;
        // do we have a (potential) banker
        if (this.bankerIndex === -1 &&
            this.players.filter(function (p) { return p && p.balance >= _this.minimumbank; }).length ===
                0) {
            console.log("no banker :'(");
            return false;
        }
        return true;
    };
    Room.prototype._beginGame = function () {
        var _this = this;
        this.status = "open";
        this.warning = -1;
        this._betActions = [];
        this._deck = __spreadArrays(allCards);
        this.coins = {}; // bank coins
        this._threecard = undefined;
        this._bankerAction = undefined;
        this.players.forEach(function (p) {
            if (p && !p.isActive) {
                p.isActive = true;
                p.__socket.leave(_this.roomnumber + "s");
                p.__socket.join(_this.roomnumber + "");
            }
        });
        // setup banker
        if (this.bankerIndex < 0) {
            var banker = this.__selectBanker();
        }
        this._broadcastToAll({ type: "srqst_ingame_gamestart" });
        db_1.default("insert into gamelogs (created_at, starting_bank) values (now(), " + this.bank + ")").then(function (_a) {
            var insertId = _a.insertId;
            _this.__gameId = insertId;
            console.log("gameid", _this.__gameId);
        });
    };
    Room.prototype.__selectBanker = function () {
        var _this = this;
        var banker = undefined;
        var historyIndex = 9999;
        this.players.forEach(function (p) {
            if (!p)
                return;
            var index = _this.__bankerHistory.indexOf(p.sid);
            if (index < historyIndex && p.balance >= _this.minimumbank) {
                banker = p;
                historyIndex = index;
            }
        });
        this.bankerIndex = banker.sid;
        banker.balance -= this.minimumbank; // TODO negative balance
        banker.bet = this.minimumbank;
        this.bank += this.minimumbank;
        this.__bankerHistory.push(banker.sid);
        return banker;
    };
    //endregion
    //region phase 1 - betting
    Room.prototype.onBet = function (_a) {
        var user = _a.user, data = _a.data;
        console.log("onBet from ", user.nickname, this.roomnumber);
        var betAmount = data.betAmount, coins = data.coins;
        this._betActions.push(__assign({ sid: user.sid }, data));
        user.bet = betAmount;
        user.balance -= betAmount;
        user._responded = true;
        this.bank += betAmount;
        console.log("bank money", this.bank);
        this._addCoins(coins);
        this._broadcastToAll({
            type: "srqst_ingame_place_bet",
            payload: __assign(__assign({ sid: user.sid }, data), { actions: this._betActions }),
        });
    };
    Room.prototype._addCoins = function (coins) {
        var _this = this;
        Object.keys(coins).map(function (coin) {
            _this.coins[coin] = (_this.coins[coin] || 0) + coins[coin];
        });
    };
    Room.prototype._didAllBet = function () {
        var _this = this;
        // everybody (but banker) has placed a bet
        return (this.players.filter(function (p) { return p && p.sid !== _this.bankerIndex && p.bet === 0; })
            .length === 0);
    };
    //endregion
    //region phase 2 - deal (card spread)
    Room.prototype._deal = function () {
        var _this = this;
        console.log("deal starting");
        this.__shuffleDeck();
        //region handout cards
        this.players.forEach(function (p) {
            if (p)
                p.cards = _this._deck.splice(0, 2);
        });
        //endregion
        console.log("check point 1");
        //region determine any winners
        var banker = this.__getBanker();
        var players = this.__getPlayers();
        players.forEach(function (p) {
            if (banker.isAutoShan()) {
                if (banker.defeats(p)) {
                    p._isRevealed = true;
                    p._result = "L";
                }
                else {
                    p._isRevealed = true;
                    p._result = "W";
                }
            }
            else {
                if (p.isAutoShan()) {
                    p._result = "W";
                    p._isRevealed = true;
                }
            }
        });
        //endregion
        console.log("check point 2");
        players.forEach(function (p) { return (p._responded = false); });
        console.log("deal ended");
        this._broadcastToAll({ type: "srqst_ingame_deal" });
    };
    Room.prototype.__shuffleDeck = function () {
        var deck = this._deck;
        for (var i = 0; i < 1000; i++) {
            var a = Math.floor(Math.random() * deck.length);
            var b = Math.floor(Math.random() * deck.length);
            var temp = deck[a];
            deck[a] = deck[b];
            deck[b] = temp;
        }
    };
    //endregion
    //region phase 3 - player action
    Room.prototype.onPlayerAction = function (_a) {
        var user = _a.user, action = _a.action;
        console.log("on player action for user", user.sid);
        user._responded = true;
        user.action = action;
        if (user.action === "draw") {
            var newCard = this._deck.pop();
            user.cards.push(newCard);
        }
    };
    //endregion
    //region phase 4 - three cards
    Room.prototype.onThreeCard = function (banker, threecard) {
        if (this.bankerIndex !== banker.sid)
            return; // ignore unauthorized attempt
        banker._responded = true;
        this._threecard = threecard;
        //region determine any winners
        console.log("threecard", threecard);
        if (threecard) {
            var players = this.__getPlayers();
            players
                .filter(function (p) { return p._result === undefined && p.cards.length === 3; })
                .forEach(function (p) {
                if (banker.defeats(p)) {
                    p._isRevealed = true;
                    p._result = "L";
                }
                else {
                    p._isRevealed = true;
                    p._result = "W";
                }
            });
        }
        //endregion
    };
    //endregion
    //region phase 5 - banker action
    Room.prototype.onBankerAction = function (banker, action) {
        console.log("banker action taken! ", action);
        if (this.bankerIndex !== banker.sid)
            return; // ignore unauthorized attempt
        banker._responded = true;
        this._bankerAction = action || "pass";
        if (action === "draw") {
            banker.cards.push(this._deck.pop());
        }
        // determine any winners
        var players = this.__getPlayers();
        players
            .filter(function (p) { return p._result === undefined; })
            .forEach(function (p) {
            if (banker.defeats(p)) {
                p._isRevealed = true;
                p._result = "L";
            }
            else {
                p._isRevealed = true;
                p._result = "W";
            }
        });
    };
    //endregion
    //region phase 6 - end game
    Room.prototype._reportResult = function () {
        // TODO sort by bet amount
        var _this = this;
        var resultplayers = [];
        this.__getPlayers().forEach(function (p) {
            // calculate winning
            if (p._result === "W") {
                p.winAmt = Math.min(p.bet * (p.getBonus() + 1), _this.bank);
                _this.bank -= p.winAmt;
            }
            else {
                p.winAmt = 0;
            }
            var balanceBefore = p.balance + p.bet;
            var balanceAfter = p.balance + p.winAmt;
            p.balance = balanceAfter;
            // save to DB
            var sql = "\n        insert into gamelog_users\n        (gameid, userid, bet, win, balanceBefore, balanceAfter, cards, isBanker) \n        values\n        (\n          '" + _this.__gameId + "', \n          '" + p.id + "', \n          '" + p.bet + "',\n          '" + p.winAmt + "',\n          '" + balanceBefore + "', \n          '" + balanceAfter + "', \n          '" + p.cards.reduce(function (p, c) { return p + c.img + c.num + " "; }, "") + "',\n          0\n        )";
            db_1.default(sql);
            db_1.default("update users set balance = " + balanceAfter + ", " + (p.winAmt > 0 ? "win = win + 1" : "lose = lose + 1") + " where id = " + p.id);
            // populate "resultPlayers"
            resultplayers.push(__assign(__assign({}, p.forJSON({ reveal: true })), { balanceBefore: balanceBefore,
                balanceAfter: balanceAfter }));
        });
        var b = this.__getBanker();
        db_1.default("insert into gamelog_users\n    (gameid, userid, bet, win, balanceBefore, balanceAfter, cards, isBanker) \n    values\n    (\n      '" + this.__gameId + "', \n      '" + b.id + "', \n      '" + b.bet + "',\n      '0',\n      '" + (b.balance + b.bet) + "', \n      '" + b.balance + "', \n      '" + b.cards.reduce(function (p, c) { return p + c.img + c.num + " "; }, "") + ", " + (this._threecard ? "3" : "") + "',\n      1\n    )");
        this._broadcastToAll({
            type: "srqst_ingame_result",
            payload: { resultplayers: resultplayers },
        });
    };
    return Room;
}());
exports.default = Room;
