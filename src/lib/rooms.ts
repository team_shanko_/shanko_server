import runQuery from "../utils/db";
import Room from "./room";
import User from "./user";

//=======================

class Rooms {
  rooms: Room[];

  constructor() {
    this.rooms = [];
  }

  async init() {
    let roomInfo = await runQuery("select * from rooms order by rank");
    let rooms: Room[] = [];
    let roomNo = 1000;
    roomInfo.forEach((row: any) => {
      for (let i = 0; i < row.num_rooms; i++)
        rooms.push(new Room(row, ++roomNo));
    });
    this.rooms = rooms;
  }

  findUserBySid(sid: number): User | undefined {
    let rooms = this.rooms;
    for (let i = 0; i < rooms.length; i++) {
      let p = rooms[i].__getPlayersAndBanker().find((p) => p.sid === sid);
      if (p)
        return p;
    }
    return undefined;
  }

  findByRoomnumber(roomnumber: number) {
    return this.rooms.find((r) => r.roomnumber === roomnumber);
  }
}

export default Rooms;
