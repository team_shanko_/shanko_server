"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var db_1 = __importDefault(require("../utils/db"));
var User = /** @class */ (function () {
    function User(user, __socket) {
        this._isDisconnected = false;
        // game related
        this.cards = [];
        this.bet = 0;
        this.isActive = false;
        this.isLeaving = false;
        this._result = undefined; //W,L
        this._isRevealed = false;
        this.seatIndex = -1;
        this._responded = false;
        this.action = undefined;
        this.winAmt = 0;
        Object.assign(this, user);
        this.sid = user.id;
        this.__socket = __socket;
        this.onGameEnd();
    }
    User.prototype.changeGender = function (gender) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.gender = gender;
                return [2 /*return*/, db_1.default("update users set gender = '" + gender + "' where id = " + this.id)];
            });
        });
    };
    User.prototype.changeImgnumber = function (imgnumber) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.imgnumber = imgnumber;
                return [2 /*return*/, db_1.default("update users set imgnumber = '" + imgnumber + "' where id = " + this.id)];
            });
        });
    };
    User.prototype.onGameEnd = function () {
        // this.isActive = false;
        this.cards = [];
        this.bet = 0;
        this.isLeaving = false;
        this._result = undefined;
        this._isRevealed = false;
        this.action = undefined;
        this.winAmt = 0;
        this._responded = true;
    };
    // onGameStart() {
    //   this.isActive = true;
    //   this._responded = true;
    // }
    User.prototype.getPt = function () {
        var sum = 0;
        this.cards.forEach(function (c) { return (sum += c.value); });
        return sum % 10;
    };
    User.prototype.isAutoShan = function () {
        return ((this.getPt() === 8 || this.getPt() === 9) && this.cards.length === 2);
    };
    User.prototype.getBonus = function () {
        var cards = this.cards;
        if (cards.length === 3) {
            if (cards[0].num === cards[1].num && cards[0].num === cards[2].num)
                return 5;
            if (cards[0].img === cards[1].img && cards[0].img === cards[2].img)
                return 3;
        }
        else if (cards.length === 2) {
            if (cards[0].img === cards[1].img)
                return 2;
        }
        return 1;
    };
    User.prototype.defeats = function (player) {
        var banker = this; // assume a subject of this call is always the banker.
        if (banker.isAutoShan() && !player.isAutoShan())
            return true;
        if (!banker.isAutoShan() && player.isAutoShan())
            return false;
        if (banker.getPt() > player.getPt())
            return true;
        if (banker.getPt() < player.getPt())
            return false;
        // compare each card separately
        var bankerCards = __spreadArrays(banker.cards).sort();
        var playerCards = __spreadArrays(player.cards).sort();
        for (var i = 0; i < Math.min(bankerCards.length, playerCards.length); i++) {
            if (bankerCards[i] > playerCards[i])
                return true;
            if (bankerCards[i] < playerCards[i])
                return false;
        }
        // banker wins all tiebreakers :D
        return true;
    };
    User.prototype.forJSON = function (_a) {
        var _b = _a.reveal, reveal = _b === void 0 ? false : _b;
        var ret = {};
        for (var key in this) {
            if (this.hasOwnProperty(key) && !key.startsWith("__")) {
                ret[key] = this[key];
            }
        }
        if (!this._isRevealed && !reveal)
            ret.cards = this.cards.map(function (c) { return ({ img: "hidden" }); });
        return ret;
    };
    return User;
}());
exports.default = User;
