import runQuery from "../utils/db";
import Card from "./card";
import { userInfo } from "os";
import Room from "./room";

interface User {
  id: number;
  nickname: string;
  balance: number;
  gender: string;
  imgnumber: number;
  win: number;
  lose: number;
  password: string;
}

class User {
  sid: number;
  __socket: SocketIO.Socket;
  __room: Room | undefined;
  _isDisconnected: boolean = false;

  // game related
  cards: Card[] = [];
  bet: number = 0;
  isActive: boolean = false;
  isLeaving: boolean = false;
  _result?: string = undefined; //W,L
  _isRevealed: boolean = false;
  seatIndex: number = -1;
  _responded: boolean = false;
  action?: string = undefined;
  winAmt: number = 0;

  constructor(user: User, __socket: SocketIO.Socket) {
    Object.assign(this, user);
    this.sid = user.id;
    this.__socket = __socket;
    this.onGameEnd();
  }
  async changeGender(gender: string) {
    this.gender = gender;
    return runQuery(
      `update users set gender = '${gender}' where id = ${this.id}`
    );
  }
  async changeImgnumber(imgnumber: number) {
    this.imgnumber = imgnumber;
    return runQuery(
      `update users set imgnumber = '${imgnumber}' where id = ${this.id}`
    );
  }
  onGameEnd() {
    // this.isActive = false;
    this.cards = [];
    this.bet = 0;
    this.isLeaving = false;
    this._result = undefined;
    this._isRevealed = false;
    this.action = undefined;
    this.winAmt = 0;
    this._responded = true;
  }
  // onGameStart() {
  //   this.isActive = true;
  //   this._responded = true;
  // }
  getPt() {
    let sum = 0;
    this.cards.forEach((c) => (sum += c.value));
    return sum % 10;
  }
  isAutoShan() {
    return (
      (this.getPt() === 8 || this.getPt() === 9) && this.cards.length === 2
    );
  }
  getBonus() {
    let cards = this.cards;
    if (cards.length === 3) {
      if (cards[0].num === cards[1].num && cards[0].num === cards[2].num)
        return 5;
      if (cards[0].img === cards[1].img && cards[0].img === cards[2].img)
        return 3;
    } else if (cards.length === 2) {
      if (cards[0].img === cards[1].img) return 2;
    }
    return 1;
  }
  defeats(player: User) {
    let banker = this; // assume a subject of this call is always the banker.
    if (banker.isAutoShan() && !player.isAutoShan()) return true;
    if (!banker.isAutoShan() && player.isAutoShan()) return false;
    if (banker.getPt() > player.getPt()) return true;
    if (banker.getPt() < player.getPt()) return false;

    // compare each card separately
    let bankerCards = [...banker.cards].sort();
    let playerCards = [...player.cards].sort();
    for (let i = 0; i < Math.min(bankerCards.length, playerCards.length); i++) {
      if (bankerCards[i] > playerCards[i]) return true;
      if (bankerCards[i] < playerCards[i]) return false;
    }
    // banker wins all tiebreakers :D
    return true;
  }
  forJSON({ reveal = false }: { reveal: boolean }) {
    let ret: any = {};
    for (let key in this) {
      if (this.hasOwnProperty(key) && !key.startsWith("__")) {
        ret[key] = this[key];
      }
    }
    if (!this._isRevealed && !reveal)
      ret.cards = this.cards.map((c) => ({ img: "hidden" }));
    return ret;
  }
}

export default User;
