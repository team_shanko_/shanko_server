import { Socket } from "socket.io";
import Card from "./card";
import User from "./user";
import { Console } from "console";
import runQuery from "../utils/db";

const INACTION_TIMEOUT = 10000; // for user-inaction
const CHECKER_INTERVAL = 500;
const COINS = [
  1000000,
  500000,
  100000,
  50000,
  20000,
  10000,
  5000,
  1000,
  500,
  100,
  50,
  10,
  5,
  1,
];

const FACES = [
  "A",
  "2",
  "3",
  "4",
  "5",
  "6",
  "7",
  "8",
  "9",
  "10",
  "J",
  "Q",
  "K",
];
const VALUES = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10];
const SUITS = ["SPADES", "HEARTS", "DIAMONDS", "CLUBS"];
const allCards: Card[] = [];
SUITS.forEach((img) => {
  FACES.forEach((num, i) => {
    allCards.push({ img, num, value: VALUES[i] });
  });
});

//region parameter interfaces
interface I_broadcastPlayers {
  excludeSid?: number;
  type: string;
  payload?: any;
}
interface I_broadcastToAll {
  excludeSid?: number;
  type: string;
  payload?: any;
}

interface IplaceBet {
  user: User;
  data: { coins: any; betAmount: number };
}
//endregion

class Room {
  static sockets: SocketIO.Socket;

  rank: number;
  minBalance: number;
  minimumbank: number;
  maxPlayers: number;
  roomnumber: number;
  players: (User | undefined)[];
  bank: number;
  _coldstart: boolean; // true: game had completely stopped. So we need 3 people to start
  _gamenumber: number = 0; // number of games played for the current banker

  // game related
  phaseIndex: number = 0;
  _subPhaseIndex: number = 0;
  status = "open";
  bankerIndex: number = -1;
  warning: number = -1;
  _betActions: any[] = [];
  _deck = [...allCards];
  coins: any = {}; // bank coins
  _threecard?: boolean;
  _bankerAction?: any;
  __bankerHistory: number[] = [];

  // db related
  __gameId: number = -1;

  constructor(config: any, roomnumber: number) {
    this.rank = config.rank;
    this.minBalance = config.min_balance;
    this.minimumbank = config.minimum_bank;
    this.maxPlayers = config.max_players;
    this.roomnumber = roomnumber;
    this.players = new Array(config.max_players).fill(undefined);
    this.bank = 0;
    this._coldstart = true;
    this._threecard = undefined;
    this._bankerAction = undefined;

    // check for events every second
    setInterval(() => {
      const banker = this.getPlayerBySid(this.bankerIndex);

      switch (this.phaseIndex * 10 + this._subPhaseIndex) {
        // join phase
        case 0:
          if (this._isReady()) {
            this._coldstart = false;
            this._setPhase(10);
          } else {
            this._coldstart = true;
          }
          break;
        // betting phase
        case 10:
          console.log("new phase", 10);
          this._beginGame();
          this._waitForRespond(this.__getPlayers());
          this._setPhase(11);
          break;
        case 11:
          if (this._didAllRespond()) this._setPhase(20);
          break;
        // deal (card-spread) phase
        case 20:
          console.log("new phase", 20);
          this._deal();
          console.log("deal complete");
          this._setPhase(21);
          break;
        case 21:
          if (this._didAllRespond()) console.log("21 is done, start phase 30");
          this._setPhase(this._isGameOver() ? 60 : 30);
          break;
        // player action phase
        case 30:
          console.log("starting phase 30");
          this._broadcastPlayers({ type: "srqst_ingame_player_action" });
          this._waitForRespond(this.__getPlayers().filter((p) => !p._result));
          this._setPhase(31);
          break;
        case 31:
          if (this._didAllRespond()) {
            console.log("sending out ingame_action_update");
            this._broadcastToAll({
              type: "srqst_ingame_player_action_update",
              payload: { actions: this._betActions },
            });
            this._waitForRespond(this.__getPlayersAndBanker());
            this._setPhase(this._isGameOver() ? 60 : 40);
          }
          break;
        // three cards phase
        case 40:
          if (this._didAllRespond()) {
            console.log("phase 40 ended");
            banker!._responded = false;
            banker!.__socket.emit(
              "srqst_ingame_three_card",
              this.forJSON(banker!.sid)
            );
            this._setPhase(41);
          }
          break;
        case 41:
          if (banker && banker._responded) {
            console.log("phase 41 ended");
            this._broadcastToAll({
              type: "srqst_ingame_three_cards",
              payload: { threecard: this._threecard },
            });
            this._waitForRespond(this.__getPlayersAndBanker());
            this._setPhase(42);
          }
          break;
        case 42:
          if (this._didAllRespond()) {
            console.log("phase 42 ended", this._isGameOver() ? 60 : 50);
            this._setPhase(this._isGameOver() ? 60 : 50);
          }
          break;
        // banker phase
        case 50:
          if (banker) {
            console.log("Phase 50 started. wait for banker action");
            banker._responded = false;
            banker.__socket.emit(
              "srqst_ingame_banker_action",
              this.forJSON(banker.sid)
            );
            this._setPhase(51);
          }
          break;
        case 51:
          if (banker && banker._responded) {
            console.log("phase 51 ended");
            this._broadcastToAll({
              type: "srqst_ingame_banker_action_update",
              payload: { action: this._bankerAction },
            });
            this._waitForRespond(this.__getPlayersAndBanker());
            this._setPhase(60);
          }
          break;
        case 60:
          // result phase
          console.log("phase 60");
          this._reportResult();
          this._waitForRespond(this.__getPlayersAndBanker());
          this._setPhase(61);
          break;
        case 61:
          if (this._didAllRespond()) {
            // leaving people

            this.players.forEach((p, i) => {
              if (!p) return;
              if (p.isLeaving) {
                p.onGameEnd();

                // leaving
                this._doLeave(p);
              }
              p.onGameEnd();
            });

            // end of banker

            // TODO Database
            this._setPhase(0);
          }
          break;
        default:
          break;
      }
    }, CHECKER_INTERVAL);
  }

  //region common
  __getBanker(): User {
    return this.players.find((p) => p && p.sid === this.bankerIndex) as User;
  }
  __getPlayers() {
    return this.players.filter(
      (p) => p !== undefined && p.sid !== this.bankerIndex && p.isActive
    ) as User[];
  }
  __getPlayersAndBanker() {
    return this.players.filter((p) => p !== undefined && p.isActive) as User[];
  }

  _setPhase(p: number) {
    this.phaseIndex = Math.floor(p / 10);
    this._subPhaseIndex = p % 10;
  }

  getPlayerBySid(sid: number) {
    return this.players.filter((p) => p).find((p) => p && p.sid === sid);
  }

  _broadcastSpectators({ type, payload = {} }: { type: string; payload: any }) {
    payload = { ...payload, ...this.forJSON() };
    try {
      Room.sockets.to(this.roomnumber + "s").emit(type, payload);
    } catch (e) {
      console.log("ERROR", e);
      console.log("ERROR payload", payload);
    }
  }

  _broadcastPlayers({ excludeSid, type, payload }: I_broadcastPlayers) {
    this.__getPlayersAndBanker().forEach((p) => {
      let piggyback = this.forJSON(p.sid);

      if (!excludeSid || p.sid !== excludeSid) {
        p.__socket.emit(type, { ...payload, ...piggyback }, console.log);
      }
    });
  }

  _broadcastToAll({ excludeSid, type, payload = {} }: I_broadcastToAll) {
    this._broadcastSpectators({ type, payload });
    this._broadcastPlayers({ excludeSid, type, payload });
  }

  forJSON(targetSid: number = -1) {
    let ret: any = {};

    for (let key in this) {
      if (this.hasOwnProperty(key)) {
        // if (key.startsWith("_")) continue;
        if (key === "players") continue;
        ret[key] = this[key];
      }
    }
    ret.players = this.players.map((p) =>
      p ? p.forJSON({ reveal: p.sid === targetSid }) : undefined
    );

    // generated properties
    ret.winners = this.players
      .filter((p) => p && p._result === "W")
      .map((p) => p!.sid);
    ret.losers = this.players
      .filter((p) => p && p._result === "L")
      .map((p) => p!.sid);
    ret.revealed = this.players
      .filter((p) => p && p._isRevealed)
      .map((p) => p!.sid);

    return ret;
  }

  onSit(user: User, seatIndex: number) {
    if (this.players[seatIndex]) {
      user.__socket.emit("resp_ingame_sit", { retcode: 1 });
      return;
    }

    this.players[seatIndex] = user;
    user.seatIndex = seatIndex;
    user.onGameEnd();

    user.__socket.emit("resp_ingame_sit", {
      retcode: 0,
      ...this.forJSON(user.sid),
    });
    this._broadcastToAll({
      excludeSid: user.sid,
      type: "srqst_ingame_newuser",
      payload: user.forJSON({ reveal: false }),
    });

    return true;
  }

  onLeaveRqst(user: User) {
    user.isLeaving = true;
    if (this.phaseIndex !== 0) return;
    if (user.sid === this.bankerIndex) return;

    this._doLeave(user);
  }
  _doLeave(user: User) {
    this._broadcastToAll({
      type: "srqst_ingame_leave",
      payload: { sid: user.sid, roomnumber: this.roomnumber },
    });

    user.__socket.leave(this.roomnumber + "s");
    user.__socket.leave(this.roomnumber + "");

    this.players[user.seatIndex] = undefined;
    user.__room = undefined;
  }

  _didAllRespond() {
    return (
      this.__getPlayersAndBanker().filter((p) => !p._responded).length === 0
    );
  }
  _waitForRespond(players: User[]) {
    players.forEach((p) => {
      if (p) p._responded = false;
    });
  }
  _isGameOver(): boolean {
    return this.__getPlayers().filter((p) => !p._result).length === 0; // everybody has a result
  }
  //endregion

  //region phase 0
  _isReady() {
    let numPlayers = this.players.filter((p) => p).length;

    if (this.roomnumber === 1001) {
      this.__getPlayers().forEach((p) => console.log(p.balance));
    }

    // do we have enough players
    if ((numPlayers < 3 && this._coldstart) || numPlayers < 2) return false;

    // do we have a (potential) banker

    if (
      this.bankerIndex === -1 &&
      this.players.filter((p) => p && p.balance >= this.minimumbank).length ===
        0
    ) {
      console.log("no banker :'(");
      return false;
    }

    return true;
  }
  _beginGame() {
    this.status = "open";
    this.warning = -1;
    this._betActions = [];
    this._deck = [...allCards];
    this.coins = {}; // bank coins
    this._threecard = undefined;
    this._bankerAction = undefined;

    this.players.forEach((p) => {
      if (p && !p.isActive) {
        p.isActive = true;
        p.__socket.leave(this.roomnumber + "s");
        p.__socket.join(this.roomnumber + "");
      }
    });

    // setup banker
    if (this.bankerIndex < 0) {
      let banker = this.__selectBanker();
    }

    this._broadcastToAll({ type: "srqst_ingame_gamestart" });
    runQuery(
      `insert into gamelogs (created_at, starting_bank) values (now(), ${this.bank})`
    ).then(({ insertId }) => {
      this.__gameId = insertId;
      console.log("gameid", this.__gameId);
    });
  }
  __selectBanker(): User {
    let banker: User | undefined = undefined;
    let historyIndex: number = 9999;

    this.players.forEach((p) => {
      if (!p) return;
      let index = this.__bankerHistory.indexOf(p.sid);
      if (index < historyIndex && p.balance >= this.minimumbank) {
        banker = p;
        historyIndex = index;
      }
    });

    this.bankerIndex = banker!.sid;
    banker!.balance -= this.minimumbank; // TODO negative balance
    banker!.bet = this.minimumbank;
    this.bank += this.minimumbank;
    this.__bankerHistory.push(banker!.sid);
    return banker!;
  }
  //endregion

  //region phase 1 - betting
  onBet({ user, data }: IplaceBet): void {
    console.log("onBet from ", user.nickname, this.roomnumber);
    const { betAmount, coins } = data;

    this._betActions.push({ sid: user.sid, ...data });
    user.bet = betAmount;
    user.balance -= betAmount;
    user._responded = true;
    this.bank += betAmount;
    console.log("bank money", this.bank);
    this._addCoins(coins);

    this._broadcastToAll({
      type: "srqst_ingame_place_bet",
      payload: {
        sid: user.sid,
        ...data,
        actions: this._betActions,
      },
    });
  }
  _addCoins(coins: any) {
    Object.keys(coins).map((coin) => {
      this.coins[coin] = (this.coins[coin] || 0) + coins[coin];
    });
  }
  _didAllBet() {
    // everybody (but banker) has placed a bet
    return (
      this.players.filter((p) => p && p.sid !== this.bankerIndex && p.bet === 0)
        .length === 0
    );
  }
  //endregion

  //region phase 2 - deal (card spread)
  _deal() {
    console.log("deal starting");
    this.__shuffleDeck();

    //region handout cards
    this.players.forEach((p) => {
      if (p) p.cards = this._deck.splice(0, 2);
    });
    //endregion

    console.log("check point 1");
    //region determine any winners
    let banker = this.__getBanker();
    let players = this.__getPlayers();
    players.forEach((p: User) => {
      if (banker.isAutoShan()) {
        if (banker.defeats(p)) {
          p._isRevealed = true;
          p._result = "L";
        } else {
          p._isRevealed = true;
          p._result = "W";
        }
      } else {
        if (p.isAutoShan()) {
          p._result = "W";
          p._isRevealed = true;
        }
      }
    });
    //endregion
    console.log("check point 2");

    players.forEach((p) => (p._responded = false));
    console.log("deal ended");
    this._broadcastToAll({ type: "srqst_ingame_deal" });
  }
  __shuffleDeck() {
    const deck = this._deck;
    for (let i = 0; i < 1000; i++) {
      let a = Math.floor(Math.random() * deck.length);
      let b = Math.floor(Math.random() * deck.length);
      let temp = deck[a];
      deck[a] = deck[b];
      deck[b] = temp;
    }
  }
  //endregion

  //region phase 3 - player action
  onPlayerAction({ user, action }: { user: User; action: string }) {
    console.log("on player action for user", user.sid);
    user._responded = true;
    user.action = action;

    if (user.action === "draw") {
      let newCard: Card | undefined = this._deck.pop();
      user.cards.push(newCard!);
    }
  }
  //endregion

  //region phase 4 - three cards
  onThreeCard(banker: User, threecard: boolean) {
    if (this.bankerIndex !== banker.sid) return; // ignore unauthorized attempt
    banker._responded = true;
    this._threecard = threecard;

    //region determine any winners
    console.log("threecard", threecard);
    if (threecard) {
      let players = this.__getPlayers();
      players
        .filter((p) => p._result === undefined && p.cards.length === 3)
        .forEach((p: User) => {
          if (banker.defeats(p)) {
            p._isRevealed = true;
            p._result = "L";
          } else {
            p._isRevealed = true;
            p._result = "W";
          }
        });
    }
    //endregion
  }
  //endregion

  //region phase 5 - banker action
  onBankerAction(banker: User, action: string) {
    console.log("banker action taken! ", action);
    if (this.bankerIndex !== banker.sid) return; // ignore unauthorized attempt

    banker._responded = true;
    this._bankerAction = action || "pass";

    if (action === "draw") {
      banker.cards.push(this._deck.pop() as any);
    }

    // determine any winners
    let players = this.__getPlayers();
    players
      .filter((p) => p._result === undefined)
      .forEach((p: User) => {
        if (banker.defeats(p)) {
          p._isRevealed = true;
          p._result = "L";
        } else {
          p._isRevealed = true;
          p._result = "W";
        }
      });
  }
  //endregion

  //region phase 6 - end game
  _reportResult() {
    // TODO sort by bet amount

    const resultplayers: any[] = [];

    this.__getPlayers().forEach((p) => {
      // calculate winning
      if (p._result === "W") {
        p.winAmt = Math.min(p.bet * (p.getBonus() + 1), this.bank);
        this.bank -= p.winAmt;
      } else {
        p.winAmt = 0;
      }
      let balanceBefore = p.balance + p.bet;
      let balanceAfter = p.balance + p.winAmt;
      p.balance = balanceAfter;

      // save to DB
      const sql = `
        insert into gamelog_users
        (gameid, userid, bet, win, balanceBefore, balanceAfter, cards, isBanker) 
        values
        (
          '${this.__gameId}', 
          '${p.id}', 
          '${p.bet}',
          '${p.winAmt}',
          '${balanceBefore}', 
          '${balanceAfter}', 
          '${p.cards.reduce((p, c) => p + c.img + c.num + " ", "")}',
          0
        )`;
      runQuery(sql);
      runQuery(`update users set balance = ${balanceAfter}, ${p.winAmt > 0 ? "win = win + 1" : "lose = lose + 1"} where id = ${p.id}`);

      // populate "resultPlayers"
      resultplayers.push({
        ...p.forJSON({ reveal: true }),
        balanceBefore,
        balanceAfter,
      });
    });

    const b = this.__getBanker();
    runQuery(`insert into gamelog_users
    (gameid, userid, bet, win, balanceBefore, balanceAfter, cards, isBanker) 
    values
    (
      '${this.__gameId}', 
      '${b.id}', 
      '${b.bet}',
      '0',
      '${b.balance + b.bet}', 
      '${b.balance}', 
      '${b.cards.reduce((p, c) => p + c.img + c.num + " ", "")}, ${
      this._threecard ? "3" : ""
    }',
      1
    )`);

    this._broadcastToAll({
      type: "srqst_ingame_result",
      payload: { resultplayers },
    });
  }
  //endregion
}

export default Room;
